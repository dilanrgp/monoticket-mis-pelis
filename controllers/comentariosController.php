<?php


require_once __DIR__ . '/../models/comentariosModel.php';

$com = new comentariosModel();

/**
 * Guarda los comentarios.
 *
 * @param string $_REQUEST['comentario']
 * @return json $comentarios
 */
if (!empty($_REQUEST['comentario'])) {
    $comentario = ($_REQUEST['comentario']) ? $_REQUEST['comentario'] : '';
    $comentarios = $com->saveComentario($_REQUEST);

    echo json_encode($comentarios);
}
