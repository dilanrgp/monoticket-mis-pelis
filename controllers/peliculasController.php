<?php
require_once __DIR__ . "/../models/peliculasModel.php";

$per = new peliculasModel();

/**
 * Busca películas segun el término de búsqueda.
 *
 * @param string $_REQUEST['termino']
 * @return json $datos
 */
if (!empty($_REQUEST['termino'])) {
    $termino = ($_REQUEST['termino']) ? $_REQUEST['termino'] : '';
    $datos = $per->getPeliculaJson($termino);

    echo json_encode($datos);
}

/**
 * Busca películas por slug.
 *
 * @param string $_REQUEST['pelicula']
 * @return view detalleView
 */
if (!empty($_REQUEST['pelicula'])) {
    $slug = ($_REQUEST['pelicula']) ? $_REQUEST['pelicula'] : '';
    $datos = $per->getPelicula($slug);

    require_once __DIR__ . "/../views/detalleView.phtml";
}

