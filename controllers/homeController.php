<?php


require_once __DIR__ . '/../models/peliculasModel.php';

$per = new peliculasModel();

/**
 * Retorna todas las películas.
 *
 * @return view peliculasView
 */
$datos = $per->getAllPeliculas();


if (isset($datos)) {
    require_once("views/peliculasView.phtml");
} else {
    require_once("views/inicioView.phtml");
}
