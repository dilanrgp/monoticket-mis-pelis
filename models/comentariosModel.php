<?php

require_once __DIR__ . "/../includes/data.php";

class comentariosModel
{
    private $db;

    public function __construct()
    {
        $this->db = Conectar::conexion();
    }

    /**
     * Guarda los comentarios.
     *
     * @param string $_REQUEST['comentario']
     * @return array $resp
     */
    public function saveComentario($formComentario)
    {

        // REGISTRAR EL COMENTARIO CON LA FECHA Y HORA ACTUAL
        $hoy = date('Y-m-d H:i:s');
        $hoy_string = $this->dateToString($hoy);
        $query = "INSERT INTO comentarios (pelicula_id, comentario, fecha) VALUES (" . $formComentario["pelicula_id"] . ",'" . $formComentario['comentario'] . "','$hoy');";
        $this->db->query($query);

        // OBTENER TODOS LOS COMENTARIOS POR ORDEN DESCENDIENTE
        $arrComentarios = array();
        $sql = $this->db->query("SELECT * FROM comentarios WHERE pelicula_id = " . $formComentario["pelicula_id"] . " ORDER BY fecha DESC;");

        // FUNCION PARA SETEAR LA FECHA
        $arrComentarios = $this->setDateComentarios($sql);

        $resp = array('status' => true, 'data' => $arrComentarios);

        return $resp;
    }

    /**
     * Recorre los comentarios para establecer la fecha en formato string. Ej: justo ahora
     *
     * @param query $comentarios
     * @return array $arrComentarios
     */
    public function setDateComentarios($comentarios)
    {
        $arrComentarios = array();
        if ($comentarios->num_rows > 0) {
            foreach ($comentarios as $row) {
                $row['fecha_string'] = $this->dateToString($row["fecha"]);
                $arrComentarios[] = $row;
            }
        }

        return $arrComentarios;
    }

    /**
     * Funcion para pasar la fecha de formato yyyy-mm-dd hh:mm:ss a formato string. Ej: hace 10 minutos
     *
     * @param string $datetime
     * @return string $string
     */

    public function dateToString($datetime)
    {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'año',
            'm' => 'mes',
            'w' => 'semana',
            'd' => 'dia',
            'h' => 'hora',
            'i' => 'minuto',
            's' => 'segundo',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        return $string ? implode(', ', $string) . ' atrás' : 'justo ahora';
    }
}
