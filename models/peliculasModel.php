<?php

require_once __DIR__ . "/../includes/data.php";
require_once __DIR__ . "/../models/comentariosModel.php";

class peliculasModel
{
    private $db;

    public function __construct()
    {
        $this->db = Conectar::conexion();
    }

    /**
     * Obtener todas las películas
     *
     * @return query $consulta
     */
    public function getAllPeliculas()
    {
        try {
            $consulta = $this->db->query("select * from peliculas;");

            return $consulta;
        } catch (\Throwable $th) {
            return null;
        }
    }

    /**
     * Obtener películas segun el término de búsqueda
     *
     * @param string $termino
     * @return array $resp
     */
    public function getPeliculaJson(string $termino)
    {
        // BUSCAR LA PELICULA SEGUN EL TÉRMINO DE BÚSQUEDA
        if (isset($termino)) {
            $sql = $this->db->query("SELECT * FROM peliculas WHERE titulo LIKE '%" . $termino . "%';");
            $resp = array('status' => true, 'data' => mysqli_fetch_all($sql, MYSQLI_ASSOC));
        } else {
            $resp = array('status' => false, 'data' => []);
        }

        return $resp;
    }

    /**
     * Obtener película según el slug único
     *
     * @param string $slug
     * @return array $datos
     */
    public function getPelicula(string $slug)
    {
        // OBTENER LA PELICULA Y SUS COMENTARIOS SEGÚN SU SLUG ÚNICO
        $consulta = $this->db->query("SELECT * FROM peliculas WHERE slug = '" . $slug . "';");
        $pelicula = $consulta->fetch_assoc();

        $comentarios = $this->db->query("SELECT * FROM comentarios WHERE pelicula_id = " . $pelicula['id'] . "  ORDER BY fecha DESC;");

        // FUNCION PARA SETEAR LA FECHA
        $comentarioModel = new comentariosModel();
        $arrComentarios = $comentarioModel->setDateComentarios($comentarios);

        $datos = array('pelicula' => $pelicula, 'comentarios' => $arrComentarios);

        return $datos;
    }
}
