$(document).ready(function() {

    $("#btn-inicio").on('click', function() {
        $.confirm({
            title: 'Iniciar Aplicación',
            content: '¿Esta seguro que desea iniciar la web?<br><small>Recuerde que debe crear la base de datos.</small>',
            type: 'blue',
            typeAnimated: true,
            buttons: {
                si: {
                    btnClass: 'btn-primary',
                    action: function () {
                        window.location.href = './';
                    }
                },
                no: function () {
                }
            }
        });
    });

    $(document).on('keypress',function(e) {
        if(e.which == 13) {
            buscarPeliculas();
        }
    });
    
    $("#btn-search").on('click', function() {
        buscarPeliculas();
    });

    $("#btn-save").on('click', function() {

        $("#inpt-comentario").removeClass('input-validation-error');
        $("#validation-comentario").html('');
        
        let comentario = $("#inpt-comentario").val();
        if (comentario.length > 3) {
            $.confirm({
                title: 'Agregar comentario!',
                content: '¿Esta seguro que desea agregar el comentario?',
                type: 'blue',
                typeAnimated: true,
                buttons: {
                    si: {
                        btnClass: 'btn-primary',
                        action: function () {
                            registrarComentario();
                        }
                    },
                    no: function () {
                    }
                }
            });
        } else {
            $("#inpt-comentario").addClass('input-validation-error');
            $("#validation-comentario").html('');
            $("#validation-comentario").append('<p class="text-danger font-italic"><small>El comentario debe tener al menos 3 caractéres</small></p>');
            $(".div-overlay").fadeOut("slow");
        }
    });
});

// BUSCAR PELICULAS
function buscarPeliculas(){
    $("#div-alert").fadeOut("slow");
    let valor = $('#inpt-search').val();

    if (valor.length > 0) {
        $(".div-overlay").fadeIn("slow");

        $.post("./controllers/peliculasController.php",{
            "termino" : valor,
        },function(resp){
            
            if (resp.status) {
                $("#div-listado").html('');
                if ( resp.data.length > 0) {
                    resp.data.forEach(item => {
                        let div = '<div class="col-xxl-3 col-lg-4 col-md-12 col-sm-12 col-xs-12 mb-3"><div class="card h-100">';
                        div += '<img src="/assets/img/'+item.portada+'" class="card-img-top" alt="'+item.titulo+'">';
                        div += '<div class="card-body">';
                        div += '<h5 class="card-title">'+item.titulo+'</h5>';
                        div += '<p class="card-text">'+item.sinopsis+'</p>';
                        div += '</div><div class="card-footer">';
                        div += '<a href="./index.php?pelicula='+item.slug+'" title="Ver Detalles" class="stretched-link text-decoration-none">Ver Detalles</a>';
                        div += '</div></div></div>';
                        $("#div-listado").append(div);
                    });
                } else {
                    $("#div-listado").append('<div class="col-12"><div class="alert alert-warning text-center" role="alert">No se encontraron registros</div></div>');
                }
                

            } else {
                $("#div-listado").html('');
                $("#div-alert").fadeIn("slow");
            }

            $(".div-overlay").fadeOut("slow");

        },"json");
    } else {
        $(".div-overlay").fadeIn("slow", function() {
            window.location.href = './';
          });
    }
}

// REGISTRAR COMENTARIOS
function registrarComentario() 
{
    $(".div-overlay").fadeIn("slow");
    $("#div-comentarios").html('');
    let dataForm = {
        comentario: $("#inpt-comentario").val(),
        pelicula_id: $("#frm-comentario").data('pelicula')
    }
    
    $.post("./controllers/comentariosController.php",
    dataForm,
    function(resp){
        if (resp.status) {
            $("#inpt-comentario").val('');
            $("#div-comentarios").html('');
            if ( resp.data.length > 0) {
                resp.data.forEach(item => {

                    let div = '<div class="card card-comment"><div class="d-flex justify-content-between align-items-center"><div class="user-comment d-flex flex-row align-items-center">';
                    div += '<img src="./assets/img/user-comment.png" width="30" class="user-img-comment rounded-circle mr-2">';
                    div += '<span><small class="font-weight-bold">'+item.comentario+'</small></span></div>';
                    div += '<small>'+item.fecha_string+'</small></div></div>';
                    $("#div-comentarios").append(div);

                });
            } else {
                $("#div-comentarios").append('<div class="col-12"><div class="alert alert-warning text-center" role="alert">No se encontraron registros</div></div>');
            }
        } else {
            $("#alert-comentario").html('');
            $("#inpt-comentario").val('');
            $("#alert-comentario").append('<div class="alert alert-danger" role="alert">Imposible registrar el comentario, intente nuevamente</div>');
        }
        $(".div-overlay").fadeOut("slow");
    },'json');
}