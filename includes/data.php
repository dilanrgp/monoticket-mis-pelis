<?php

class Conectar
{
    public static function conexion()
    {
        // VARIABLES DE CONEXION A LA BASE DE DATOS
        $db_host = '127.0.0.1';
        $db_port = 3306;
        $db_database = 'monoticket_database';
        $db_username = 'root';
        $db_password = '';

        try {
            $conexion = new mysqli($db_host, $db_username, $db_password, $db_database, $db_port);
            $conexion->query("SET NAMES 'utf8'");
            return $conexion;
        } catch (\Throwable $th) {
            return null;
        }

       
        
        
    }
}